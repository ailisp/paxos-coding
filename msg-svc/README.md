Challenge #1 Usage:
1. Install docker and docker-compose
2. `docker-compose build`
3. `docker-compose up`
4. Test the service as the question suggested

Scaling Question:
I'd deploy the app as a kubernetes deployment, giving the built docker image and specify the `replicas` of the `ReplicaSet` field in deployment spec. Increase the `replicas` manually to handle more user requests. Another bottleneck would be concurrent access to Redis. I would also deploy Redis with Kubernetes helm chart. From there I can specify the number of slaves in Redis cluster to increase throughput.

Deployment Question:
This is partially answered in scaling question part. I'll use Kubernetes deployment so I can update and rollback the deployment. Also I'd use a Jenkinsfile which does the docker build, run tests and trigger kubernetes deployment set image so there's an automatic pipeline in commit/merge PR, build and deployment. Also the automatic deployment is going to staging server if a higher reliability is required, and manually triggered to production after Q/A.

import os
import hashlib

from flask import Flask, request, jsonify
from flask_redis import Redis

import config

current_config = config.config[os.environ.get("FLASK_ENV", "production")]
app = Flask(__name__)
app.config.from_object(current_config)
redis = Redis(app)

def err_msg(msg):
    res = jsonify({"err_msg": msg})
    res.status_code = 400
    return res

@app.route('/messages', methods=["POST"])
def post_message():
    try:
        msg = request.get_json()["message"]
    except KeyError:
        return err_msg("Invalid input format")
    digest = hashlib.sha256(
        msg.encode('utf-8')).hexdigest()
    redis.set(digest, msg)
    return jsonify({"digest": digest})

@app.route('/messages/<digest>')
def get_message(digest):
    msg = redis.get(digest)
    if msg is None:
        return err_msg("Message not found")
    return jsonify({"message": msg.decode(encoding='utf-8')})

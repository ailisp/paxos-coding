Time complexity is O(n) since it only does constant operation inside the loop.
For the bonus question, there's a nested loop in find-pair3, and constant operation inside the loop, so that time complexity is O(n^2)
